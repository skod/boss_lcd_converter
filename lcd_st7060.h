/* 
 * File:   lcd.h
 * Author: Student
 * Created on 14 september 2016, 11:10`
 */

#ifndef LCD_H
#define	LCD_H
/* PIC18 XLCD peripheral routines.
 *
 *   Notes:
 *      - These libraries routines are written to support the
 *        Hitachi HD44780 LCD controller.
 *      - The user must define the following items:
 *          - The LCD interface type (4- or 8-bits)
 *          - If 4-bit mode
 *              - whether using the upper or lower nibble
 *          - The data port
 *              - The tris register for data port
 *              - The control signal ports and pins
 *              - The control signal port tris and pins
 *          - The user must provide three delay routines:
 *              - DelayFor18TCY() provides a 18 Tcy delay
 *              - DelayPORXLCD() provides at least 15ms delay
 *              - DelayXLCD() provides at least 5ms delay
 */

/* Interface type 8-bit or 4-bit
 * For 8-bit operation uncomment the #define BIT8
 */
 #define BIT8 

/* When in 4-bit interface define if the data is in the upper
 * or lower nibble.  For lower nibble, comment the #define UPPER
 */
/* #define UPPER */

/* DATA_PORT defines the port to which the LCD data lines are connected */
 

/* DATA_PORT defines the port to which the LCD data lines are connected */
 #define DATA_PORT      		PORTA
 #define TRIS_DATA_PORT 		TRISA

/* CTRL_PORT defines the port where the control lines are connected.
 * These are just samples, change to match your application.
 */

 #define RS_PIN   LATCbits.LATC0   		/* PORT for RS */
 #define TRIS_RS  TRISCbits.TRISC0    	/* TRIS for RS */

 #define RW_PIN   LATCbits.LATC1   		/* PORT for RW */
 #define TRIS_RW  TRISCbits.TRISC1    	/* TRIS for RW */

 #define E_PIN    LATCbits.LATC2  		/* PORT for D  */
 #define TRIS_E   TRISCbits.TRISC2    	/* TRIS for E  */

/* Display ON/OFF Control defines */
#define DON         0b00001111  /* Display on      */
#define DOFF        0b00001011  /* Display off     */
#define CURSOR_ON   0b00001111  /* Cursor on       */
#define CURSOR_OFF  0b00001101  /* Cursor off      */
#define BLINK_ON    0b00001111  /* Cursor Blink    */
#define BLINK_OFF   0b00001110  /* Cursor No Blink */

/* Cursor or Display Shift defines */
#define SHIFT_CUR_LEFT    0b00000100  /* Cursor shifts to the left   */
#define SHIFT_CUR_RIGHT   0b00000110  /* Cursor shifts to the right  */
#define SHIFT_DISP_LEFT   0b00000111  /* Display shifts to the left  */
#define SHIFT_DISP_RIGHT  0b00000101  /* Display shifts to the right */


/* Function Set defines */
#define FOUR_BIT   0b00101100  /* 4-bit Interface               */
#define EIGHT_BIT  0b00111100  /* 8-bit Interface               */
#define LINE_5X7   0b00110000  /* 5x7 characters, single line   */
#define LINE_5X10  0b00110100  /* 5x10 characters               */
#define LINES_5X7  0b00111000  /* 5x7 characters, multiple line */

#define PARAM_SCLASS auto
#define MEM_MODEL far  /* Change this from far to near for small memory model */

/* OpenGLCD
 * Configures I/O pins for external LCD
 */
void OpenGLCD(PARAM_SCLASS unsigned char);

/* SetCGRamAddr
 * Sets the character generator address
 */
void SetCGRamAddr(PARAM_SCLASS unsigned char);

/* SetDDRamAddr
 * Sets the display data address
 */
void SetDDRamAddr(PARAM_SCLASS unsigned char);

/* BusyGLCD
 * Returns the busy status of the LCD
 */
unsigned char BusyGLCD(void);

/* ReadAddrGLCD
 * Reads the current address
 */
unsigned char ReadAddrGLCD(void);

/* ReadDataGLCD
 * Reads a byte of data
 */
char ReadDataGLCD(void);

/* WriteCmdGLCD
 * Writes a command to the LCD
 */
void WriteCmdGLCD(PARAM_SCLASS unsigned char);

/* WriteDataGLCD
 * Writes a data byte to the LCD
 */
void WriteDataGLCD(PARAM_SCLASS char);

/* putcGLCD
 * A putc is a write
 */
#define putcGLCD WriteDataGLCD

/* putsGLCD
 * Writes a string of characters to the LCD
 */
void putsGLCD(PARAM_SCLASS char *);

/* putrsGLCD
 * Writes a string of characters in ROM to the LCD
 */
void putrsGLCD(const MEM_MODEL rom char *);

/* User defines these routines according to the oscillator frequency */
void DelayFor18TCY(void);
void DelayPORGLCD(void);
void DelayGLCD(void);
#endif	/* LCD_H */

