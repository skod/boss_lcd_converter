/*
 * File:   main.c
 * Author: Viktor & Oleksandr
 * Created on 16 July 2022, 02:17
 * for lcd WH2001b/WH2002L on st7066 controller
 * https://www.winstar.com.tw/ru/products/character-lcd-display-module/20x2-datasheet.html
 * usart 19200 bps, stop bit 1, no flow control
 */

#include <p18F26K22.h>
#include <stdio.h>
#include <stdlib.h>
#include "lcd_st7060.h"
#include <usart.h>
#include <delays.h>
#include "main.h"

#pragma config FOSC = INTIO67   // Oscillator Selection bits (Internal oscillator block)
#pragma config PLLCFG = OFF     // 4X PLL Enable (Oscillator used directly)
#pragma config PRICLKEN = OFF    // Primary clock enable bit (Primary clock enabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config WDTEN = OFF

void blink();
void init_lcd(void);
void processCommand(char* command);
void init_hardware(void);
int backlight(char val);
int selectLine(int line);

char error_type = 0;
const int displayLen = 19; // 0-19
void rx_handler(void);

char i = 0;

#pragma code rx_interrupt = 0x8

void rx_int(void) {

    _asm
            goto rx_handler
            _endasm

}
#pragma code
char cmd[5];
#pragma interrupt rx_handler

void rx_handler(void) {
    cmd[i] = getc2USART();
    if (cmd[0] != 0xe7)
        return;

    i++;
    if (i == 4) {
        i = 0;
        processCommand(cmd);
        while (Busy2USART());
        Write2USART(cmd[0]);

        while (Busy2USART());
        Write2USART(cmd[1]);

        while (Busy2USART());
        Write2USART(cmd[2]);

        while (Busy2USART());
        Write2USART(cmd[3]);
    }

    PIR3bits.RC2IF = 0;
}

void init_hardware() {
    OSCCON = 0b01111100;

    INTCONbits.GIE = 1; // Global interupts enabled.    //   //
    INTCONbits.PEIE = 1; // Peripheral interupts enabled.//   //
    RCONbits.IPEN = 1; // Enable priority levels.      //   //
    // RCSTAbits.SPEN = 1;             // Serial port enabled.
    IPR1bits.RC1IP = 1;
    IPR3bits.RC2IP = 1;

    PORTA = 0;
    PORTB = 0;
    PORTC = 0;

    LATA = 0b00000000;
    ANSELA = 0b00000000;

    LATB = 0b00000000;
    ANSELB = 0b00000000;

    TRISA = 0b00000000;
    TRISB = 0b11000000;
    TRISC = 0b11000000;

    Open1USART(USART_TX_INT_OFF &
            USART_RX_INT_OFF &
            USART_ASYNCH_MODE &
            USART_EIGHT_BIT &
            USART_CONT_RX &
            USART_BRGH_LOW,
            12); //12 for 19200 -  16 MHz  for debug info
    //12 for 9600 -  8 MHz  for debug info
    //25 for 19200 -  8 MHz  for debug info
    Open2USART(USART_TX_INT_OFF &
            USART_RX_INT_ON &
            USART_ASYNCH_MODE &
            USART_EIGHT_BIT &
            USART_CONT_RX &
            USART_BRGH_LOW,
            12); //25  for  9600 - 16 MHz for receive data

    putrs1USART("init hardware\r\n>");
}

void blink() {
    PORTCbits.RC3 = 1;
    Delay1KTCYx(10); // 1 ms at 16MHz  = (ms*MHz/4)
    PORTCbits.RC3 = 0;
    Delay10KTCYx(200); // 40 ms at 16MHz
    Delay10KTCYx(200); // 40 ms at 16MHz
    return;
}

void writeOneCharacter(char buffer) {
    while (BusyGLCD()); // Wait while LCD is busy
    WriteDataGLCD(buffer); // Write character to LCD

}

void init_lcd() {
    putrs1USART("start init LCD\r\n>");
    OpenGLCD(EIGHT_BIT & LINES_5X7);

    while (BusyGLCD()); // Wait if LCD busy
    WriteCmdGLCD(DON & CURSOR_OFF & BLINK_OFF); // Display ON/Blink ON

    while (BusyGLCD());
    SetDDRamAddr(0x00);

    i = 0;
    cmd[4] = '\0';
    putrs1USART("init LCD finished\r\n>");
    return;
}

void upadte_lcd(char *buff) {
    putsGLCD(buff);
    return;
}

void processCommand(char* command) {
    static int cnt = 0;
    if (command[1] == 0x44) {
        writeOneCharacter(command[2]);
        cnt++;
        if (cnt > displayLen) {
            selectLine(2);
            cnt = 0;
        }

    } else if (command[1] == 0x43) {
        while (BusyGLCD());
        WriteCmdGLCD(CLEAR_DISPLAY);
        cnt = 0;
    } else if (command[1] == 0x42) {
        backlight(command[2]);
    }
}

int selectLine(int line) {
    if (line == 2) {
        while (BusyGLCD());
        SetDDRamAddr(0x40);
        return 0;
    }

    while (BusyGLCD());
    SetDDRamAddr(0x00);
    return 0;
}

int backlight(char val) {
    if (val == 1) {
        PORTBbits.RB0 = 0;
    } else {
        PORTBbits.RB0 = 1;
    }
}

void main() {

    init_hardware();
    PORTCbits.RC3 = 1;
    Delay10KTCYx(9000000); // 40 ms at 16MHz
    PORTCbits.RC3 = 0;
    init_lcd();

    while (1) {
        blink();
    }// end while
}
